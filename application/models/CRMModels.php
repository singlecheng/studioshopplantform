<?php
class CRMModels extends CI_Model {

        // public $title;
        // public $content;
        // public $date;

        public function __construct()
        {
                // Call the CI_Model constructor
                parent::__construct();
        }

        public function GetMemberList()
        {
                $sql = $this->db->query('SELECT * FROM `users` WHERE `role` = "member"');
                return $sql->result();
        }
}
?>