<?php
class OrderModels extends CI_Model {

        // public $title;
        // public $content;
        // public $date;

        public function __construct()
        {
                // Call the CI_Model constructor
                parent::__construct();
        }

        public function GetOrderGroupList()
        {
                $sql = $this->db->query('SELECT * FROM `ordergroup` ORDER BY `ordergroup`.`CeateTime` DESC');
                return $sql->result();
        }

        public function GetOrderGroupCount()
        {
                $sql = $this->db->query('SELECT ( SELECT COUNT(*) FROM ordergroup ) AS count1, ( SELECT COUNT(*) FROM orderlist ) AS count2 FROM dual');
                return $sql->result();
        }

        public function GetMenuProductList()
        {
                $sql = $this->db->query('SELECT * FROM  `menuproduct` ');
                return $sql->result();
        }

        public function GetProductTotal()
        {
                $sql = $this->db->query('SELECT count(*) as total from `menuproduct`');
                return $sql->result();
        }

        public function GetOrderGroupID()
        {
                $sql = $this->db->query('SELECT `Id` FROM  `ordergroup` ORDER BY  `ordergroup`.`Id` DESC LIMIT 0 , 1');
                return $sql->result();
        }

        public function GetOrderGroupDetail()
        {
                $sql = $this->db->query('SELECT * FROM `ordergroup`');
                return $sql->result();
        }

        public function GetOrderDetail()
        {
                $sql = $this->db->query('SELECT * FROM `orderlist`');
                return $sql->result();
        }
}
?>