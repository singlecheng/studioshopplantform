<?php
class AccountModels extends CI_Model {

        // public $title;
        // public $content;
        // public $date;

        public function __construct()
        {
                // Call the CI_Model constructor
                parent::__construct();
        }

        public function GetUsersList()
        {
                $sql = $this->db->query('SELECT * FROM `users`');
                return $sql->result();
        }

        public function GetLogList()
        {
                $sql = $this->db->query('SELECT * FROM `accountlog` ORDER BY `accountlog`.`time` DESC LIMIT 50');
                return $sql->result();
        }
}
?>