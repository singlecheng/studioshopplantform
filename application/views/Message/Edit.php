<!DOCTYPE html>
<html>
<head>
<title>Reply Mail</title>
<meta charset="utf-8">
<!--loag Assets  -->
<?php $this->load->view('pageassets');?>

</head>
<body>
	<!-- load navibar -->
<?php $this->load->view('navibar');?>

	<div class="container">
		<div class="jumbotron">
			<h1>Reply Mail</h1>
			<p></p>
		</div>
		<div class="form-horizontal">
			<a href="/BeaconOrderServer/index.php/Message">Back to list</a>

			<?php
			$id = $_POST ["submit"];
			$servername = "localhost";
			$username = "root";
			$password = "";
			$dbname = "bryan_beaconorder";
			
			// Create connection
			$conn = new mysqli ( $servername, $username, $password, $dbname );
			
			// 資料回傳編碼設UTF8
			mysqli_set_charset ( $conn, "utf8" );
			
			// Check connection
			if ($conn->connect_error) {
				die ( "Connection failed: " . $conn->connect_error );
			}
			
			$sql = "SELECT * FROM `msgboard` WHERE Id = $id";
			$result = $conn->query ( $sql );
			
			if ($result->num_rows > 0) {
				?><table class="table">
				<thead>
					<tr>
						<th>ID</th>
						<th>Subject</th>
						<th>Context</th>
						<th>E-mail</th>
						<th>TIME</th>
					</tr>
				</thead>
				<tbody>
				<?php
				// output data of each row
				while ( $row = $result->fetch_assoc () ) {
					$email = $row ["Email"];
					echo "<tr><td>" . $row ["Id"] . "</td><td>" . $row ["Subject"] . "</td><td>" . $row ["Context"] . "</td><td>" . $row ["Email"] . "</td><td>" . $row ["TIME"] . "</td></tr>";
				}
				?></tbody>
			</table><?php
			} else {
				echo "0 results";
			}
			
			$conn->close ();
			?>
			
			<hr>

			<form method="POST" action="/BeaconOrderServer/index.php/Shop/Delete">

				<div class="form-group">
					<label class="control-label col-md-2">Id</label>
					<div class="col-md-10">
	                <?php echo "<input class=form-control text-box single-line id=Id name=Id type=text value=$id>"?>
	                <span class="field-validation-valid text-danger"
							data-valmsg-for="Name" data-valmsg-replace="true"></span>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-offset-2 col-md-10">
						<input type="submit" name="Submit" class="btn btn-danger"
							value="Delete">
					</div>
				</div>
			</form>

			<hr>

			<form name="user" method="POST"
				action="/BeaconOrderServer/index.php/Message/Send">

				<div class="form-group">
					<label class="control-label col-md-2">Id</label>
					<div class="col-md-10">
	                <?php echo "<input class=form-control text-box single-line id=Id name=Id type=text value=$id>"?>
	                <span class="field-validation-valid text-danger"
							data-valmsg-for="Name" data-valmsg-replace="true"></span>
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-md-2"><span
						class="glyphicon glyphicon-envelope"></span> E-mail</label>
					<div class="col-md-10">
	                <?php echo "<input class=form-control text-box single-line id=email name=email type=text value=$email>"?>
	                <span class="field-validation-valid text-danger"
							data-valmsg-for="email" data-valmsg-replace="true"></span>
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-md-2"><span
						class="glyphicon glyphicon-pencil"></span> Context<br>(Select
						Subject)</label>
					<div class="col-md-10" ng-app="myApp" ng-controller="myCtrl">
						<select class="form-control" ng-model="firstName">
							<option selected="selected" ng-model="firstName">訂單問題 :</option>
							<option ng-model="firstName">店家資訊問題 :</option>
							<option ng-model="firstName">菜單問題 :</option>
							<option ng-model="firstName">桌號問題 :</option>
							<option ng-model="firstName">優惠問題 :</option>
							<option ng-model="firstName">帳戶問題 :</option>
							<option ng-model="firstName">硬體問題 :</option>
							<option ng-model="firstName">其他 :</option>
						</select>
						<textarea class="form-control" rows="5" id="Context"
							name="Context" value='{{firstName}}'>{{firstName}}</textarea>
					</div>

					<script>
                    var app = angular.module('myApp', []);
                    app.controller('myCtrl', function($scope) {
                        $scope.firstName = "";
                    });
                    </script>
				</div>

				<div class="form-group">
					<div class="col-md-offset-2 col-md-10">
						<input type="submit" name="Submit" class="btn btn-info"
							value="Reply Mail">
					</div>
				</div>
			</form>
		</div>
	</div>
</body>
</html>