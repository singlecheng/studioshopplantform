<!DOCTYPE html>
<html lang="en">
<head>
<title>Beacon Order Server - Menu</title>
<meta charset="utf-8">
<!--loag Assets  -->
<?php $this->load->view('pageassets');?>

</head>
<body>
	<!-- load navibar -->
<?php $this->load->view('navibar');?>

	<div class="container">
		<div class="jumbotron">
			<h1>Menu List</h1>
            <a href="/studioshopplantform/index.php/Menu/Create" class="btn btn-primary btn-lg"><span class="glyphicon glyphicon-plus-sign"></span> Create</a>
			<p></p>

		</div>

		<div>
        <table class="table table-striped">
				<thead>
					<tr>
						<th>ID</th>
						<th>Name</th>
						<th>type</th>
						<th>price</th>
						<th>Shop</th>
						<th>ShopId</th>
					</tr>
				</thead>
				<tbody>
                    <?php
                        foreach ($MenuList as $List) {
                            echo "<tr><td>" . $List->Id . "</td><td>" . $List->Name . "</td><td>" . $List->type . "</td><td>" . $List->price . "</td><td>" . $List->Shop . "</td><td>" . $List->ShopId . "</td></tr>" ;
                        }
                    ?>
                </tbody>
                    </table>
                    </div>
            </div>
		
    <!-- load footer -->
	<?php $this->load->view('pagefooter');?>
</body>
</html>