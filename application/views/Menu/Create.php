<!DOCTYPE html>
<html lang="en">
<head>
<title>Studio Shop Plantform - Account Login</title>
<meta charset="utf-8">
<!--loag Assets  -->
<?php $this->load->view('pageassets');?>

</head>
<body>
<!-- load navibar -->
<?php $this->load->view('navibar');?>

	<div class="container">
		<div class="jumbotron">
			<h1>Order Create</h1>
			
			<p></p>
		</div>
		<div class="form-horizontal">
	
	<div class="container">
		<div class="form-horizontal">
			<hr>

			<form name="user" method="POST" action="">
				<div class="form-group">
					<label class="control-label col-md-2">Product Name</label>
					<div class="col-md-10">
						<input class="form-control text-box single-line" id="Name"
							name="Name" type="text" value=""> <span
							class="field-validation-valid text-danger"
							data-valmsg-for="Name" data-valmsg-replace="true"></span>
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-md-2">Type</label>
					<div class="col-md-10">
						<input class="form-control text-box single-line"
							id="Type" name="Type" type="text" value="">
						<span class="field-validation-valid text-danger"
							data-valmsg-for="Type" data-valmsg-replace="true"></span>
					</div>
				</div>

                <div class="form-group">
					<label class="control-label col-md-2">Price</label>
					<div class="col-md-10">
						<input class="form-control text-box single-line" id="Price" name="Price" type="text" value=""> <span
							class="field-validation-valid text-danger"
							data-valmsg-for="Price" data-valmsg-replace="true"></span>
					</div>
				</div>

				<div class="form-group">
					<div class="col-md-offset-2 col-md-10">
						<input type="submit" name="Submit" class="btn btn-success" value="Submit">
					</div>
				</div>
			</form>

		</div>
	</div>

	<!-- load footer -->
	<?php $this->load->view('pagefooter');?>
</body>
</html>
		