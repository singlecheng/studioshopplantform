<!-- page load -->
<!DOCTYPE html>
<html>
<head>
<title>Beacon Order Server - CRM</title>
<meta charset="utf-8">
<!--loag Assets  -->
<?php $this->load->view('pageassets');?>

</head>
<body>
	<!-- load navibar -->
<?php $this->load->view('navibar');?>

	<div class="container">
		<div class="jumbotron">
			<h1>Product Edit</h1>
			<p></p>
		</div>
		<div>
		<table class="table table-striped">
		<form name="user" method="POST" action="/studioshopplantform/index.php/Menu/Edit">
			<thead>
				<tr>
					<th>ID</th>
					<th><span class="glyphicon glyphicon-tag"></span> Name</th>
					<th>Type</th>
					<th>Price</th>
					<th><span class="glyphicon glyphicon-edit"></span> Edit</th>
				</tr>
			</thead>
			<tbody>
				<?php
					foreach ($MenuList as $List) {
						$num = $List->Id;
                        if($num == $_GET['PId'])
                        {
    						echo "<tr><td>" . $List->Id . "</td><td>" . $List->Name . "</td><td>" . $List->type . "</td><td>" . $List->price . "</td><td>" . "<input type='submit' name='PId' class='btn btn-warning' value=$num ></td></tr>";
                        }
					}
				?>
			</tbody>
			</form>
		</table>
		</div>
	</div>
</body>
</html>