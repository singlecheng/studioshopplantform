<nav class="navbar navbar-inverse">
	<div class="container-fluid">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse"
				data-target="#myNavbar">
				<span class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="/index.php">StudioShopPlantform</a>
		</div>
		<div class="collapse navbar-collapse" id="myNavbar">
			<ul class="nav navbar-nav">
				<li><a href="/studioshopplantform/index.php"><span class="glyphicon glyphicon-home"></span> Home</a></li>
				<li><a href="/studioshopplantform/index.php/Order"><span class="glyphicon glyphicon-shopping-cart"></span> Order</a></li>
				<li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#"><span class="glyphicon glyphicon-th-list"></span> Shop<span class="caret"></span></a>
					<ul class="dropdown-menu">
					<!--
						<li><a href="/studioshopplantform/index.php/Shop">Shop</a></li>
					-->
						<li><a href="/studioshopplantform/index.php/Menu">Menu</a></li>
					</ul></li>
					<!--
				<li><a href="/studioshopplantform/index.php/Promotion"><span class="glyphicon glyphicon-tags"></span> Promotion</a></li>
				<li><a href="/studioshopplantform/index.php/Beacon"><span class="glyphicon glyphicon-bitcoin"></span> Beacon</a></li>
				<li><a href="/studioshopplantform/index.php/Message"><span class="glyphicon glyphicon-cloud-upload"></span> Msg Board</a></li>
					-->
				<li><a href="/studioshopplantform/index.php/ERP"><span class="glyphicon glyphicon-stats"></span> Data Analysis</a></li>
				<li><a href="/studioshopplantform/index.php/CRM"><span class="glyphicon glyphicon-duplicate"></span> CRM</a></li>
				<li><a href="/studioshopplantform/index.php/Account"><span class="glyphicon glyphicon-cog"></span> Plantform Manage</a></li>
			</ul>
			<ul class="nav navbar-nav navbar-right">
					<!-- session load -->
					<?php
					if (isset($_SESSION ['username'])) {
						?>
							<li><a href=""><span class="glyphicon glyphicon-user"></span>User : <?php echo $_SESSION['username'];?></a></li>
							<li><a href="/studioshopplantform/index.php/Account/logout"><span class="glyphicon glyphicon-log-in"></span> Logout</a></li>
						<?php
					} 

					else {
						?>
							<li><a href="/studioshopplantform/index.php/Account/Register"><span class="glyphicon glyphicon-log-in"></span> Register</a></li>
							<li><a href="/studioshopplantform/index.php/Account/Login"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
						<?php
					}
					;
					?>
				</ul>
		</div>
	</div>
</nav>