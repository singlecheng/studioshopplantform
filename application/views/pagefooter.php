<hr>
<footer class="container-fluid bg-4 text-center">
	<p>
		Power By : <a href="https://www.android.com/">Android</a> | <a
			href="https://angularjs.org/">AngularJS</a> | <a
			href="https://bitbucket.org/">Bitbucket</a> | <a
			href="http://getbootstrap.com/">Bootstrap</a> | <a
			href="http://codeigniter.org.tw/">CodeIgniter</a> | <a
			href="http://d3js.org/">D3</a> | <a
			href="https://eclipse.org/downloads/">Eclipse</a> | <a
			href="http://estimote.com/">Estimote</a> | <a
			href="https://github.com/">GitHub</a> | <a
			href="https://developer.apple.com/ibeacon/">iBeacon</a> | <a
			href="https://jquery.com/">jQuery</a> | <a
			href="http://www.json.org/">JSON</a> | <a
			href="https://www.mysql.com/downloads/">MySQL</a> | <a
			href="http://php.net/downloads.php">PHP</a> | <a
			href="https://www.sourcetreeapp.com/">SourceTree</a> | <a
			href="https://www.apachefriends.org/zh_tw/index.html">Xampp</a> |
	</p>
	<p class="footer_copyright">
		© 2016 <a href="mailto:singleqiang@gmail.com">Single Studio</a>. All
		Rights Reserved.
	</p>
</footer>
</hr>