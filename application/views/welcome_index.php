<!DOCTYPE html>
<html lang="en">
<head>
<title>Beacon Order Server</title>
<meta charset="utf-8">
<link href="/studioshopplantform/assets/image/favicon.ico" rel="SHORTCUT ICON">
<!--loag Assets  -->
<?php $this->load->view('pageassets');?>
<!-- Carousel Plugin -->
<style>
.carousel-inner>.item>img, .carousel-inner>.item>a>img {
	margin: auto;
}

.carousel {
	width: 100%;
	margin: auto;
}

.carousel-control.right, .carousel-control.left {
	background-image: none;
	color: #1abc9c;
}

.carousel-indicators li {
	border-color: #1abc9c;
}

.carousel-indicators li.active {
	background-color: #1abc9c;
}
</style>

<!-- navi bar -->
<style>
.bg-1 {
	background-color: #1abc9c;
	color: #ffffff;
}

.bg-2 {
	background-color: #474e5d;
	color: #ffffff;
}

.bg-3 {
	background-color: #ffffff;
	color: #555555;
}

.navbar-nav  li a:hover {
	color: #1abc9c !important;
}

.jumbotron {
	background-color: #00bbff;
	color: #fff;
	padding: 1px 25px;
	font-family: Montserrat, sans-serif;
}
</style>

</head>
<body>
	<!-- load navibar -->
<?php $this->load->view('navibar');?>

	<div class="jumbotron text-center">
		<h1>
			<b>Studio Shop Plantform</b>
		</h1>

	</div>

	<div class="container text-center ">
		<div class="panel-heading">
			<h2>
				<b><span class="glyphicon glyphicon-list-alt"></span> Project
					Prototype</b>
			</h2>
		</div>
		<div class="row">
			<div class="col-md-4">
				<h3>
					<p class="bg-danger">
						<span class="glyphicon glyphicon-picture"></span> Why ?
					</p>
				</h3>
			</div>
			<div class="col-md-4">
				<h3>
					<p class="bg-info">
						<span class="glyphicon glyphicon-picture"></span> Do
					</p>
				</h3>
			</div>
			<div class="col-md-4">
				<h3>
					<p class="bg-success">
						<span class="glyphicon glyphicon-picture"></span> So
					</p>
				</h3>
			</div>
		</div>
	</div>
	<!-- load footer -->
	<?php $this->load->view('pagefooter');?>
</body>
</html>