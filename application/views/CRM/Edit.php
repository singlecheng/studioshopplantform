<!DOCTYPE html>
<html>
<head>
<title>CRM - Send AD</title>
<meta charset="utf-8">
<!--loag Assets  -->
<?php $this->load->view('pageassets');?>
<script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>
</head>
<body>
	<!-- load navibar -->
<?php $this->load->view('navibar');?>

	<div class="container">
		<div class="jumbotron">
			<h1>Mail Edit</h1>
			<p></p>

		</div>
		<div class="form-horizontal">
		<table class="table table-striped">
			<thead>
				<tr>
					<th>ID</th>
					<th><span class="glyphicon glyphicon-tag"></span> Name</th>
					<th><span class="glyphicon glyphicon-envelope"></span> E-mail</th>
				</tr>
			</thead>
			<tbody>
				<?php
					foreach ($MemberList as $List) {
						$num = $List->id;
						$email = $List->email;
						echo "<tr><td>" . $List->id . "</td><td>" . $List->Name . "</td><td>" . $List->email . "</td></tr>";
					}

				?>
			</tbody>
			</table>
			
			<hr>

			<form method="POST" action="/studioshopplantform/index.php/CRM/Send">

				<div class="form-group">
					<label class="control-label col-md-2">Id</label>
					<div class="col-md-10">
	                <?php echo "<input class=form-control text-box single-line id=Id name=Id type=text value='$num'>"?>
	                <span class="field-validation-valid text-danger"
							data-valmsg-for="Name" data-valmsg-replace="true"></span>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-2"><span
						class="glyphicon glyphicon-envelope"></span> E-mail</label>
					<div class="col-md-10">
	                <?php echo "<input class=form-control text-box single-line id=email name=email type=text value='$email'>"?>
	                <span class="field-validation-valid text-danger"
							data-valmsg-for="email" data-valmsg-replace="true"></span>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-2"><span
						class="glyphicon glyphicon-pencil"></span> Context</label>
					<div class="col-md-10" ng-app="myApp" ng-controller="myCtrl">
						<input class="form-control text-box single-line" id="Context"
							name="Context" type=text
							value='{{firstName + " " + lastName + " " + lastName2}}' />
						<P>(context select)</P>
						<select class="form-control" ng-model="firstName">
							<option selected="selected" ng-model="firstName">周年慶</option>
							<option ng-model="firstName">限時大優惠</option>
							<option ng-model="firstName">店長生日</option>
							<option ng-model="firstName">聖誕節</option>
							<option ng-model="firstName">過年優惠</option>
						</select> <select class="form-control" ng-model="lastName">
							<option selected="selected" ng-model="lastName">全面9折</option>
							<option ng-model="lastName">全面8折</option>
							<option ng-model="lastName">全面7折</option>
							<option ng-model="lastName">全面6折</option>
							<option ng-model="lastName">全面5折</option>
						</select> </select> <select class="form-control"
							ng-model="lastName2">
							<option selected="selected" ng-model="lastName2">搶便宜要快</option>
							<option ng-model="lastName">優惠只到月底哦 !</option>
							<option ng-model="lastName">來店消費就送好禮</option>
							<option ng-model="lastName">還有更多優等你來看</option>
							<option ng-model="lastName">不要錯過</option>
						</select>
					</div>

					<script>
                    var app = angular.module('myApp', []);
                    app.controller('myCtrl', function($scope) {
                        $scope.firstName = "";
                        $scope.lastName = "";
                        $scope.lastName2 = "";
                    });
                    </script>
				</div>
				<div class="form-group">
					<div class="col-md-offset-2 col-md-10">
						<input type="submit" name="Submit" class="btn btn-info"
							value="Send Ad">
					</div>
				</div>
			</form>

			<hr>
		</div>
	</div>
</body>
</html>