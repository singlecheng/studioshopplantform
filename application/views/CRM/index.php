﻿<!-- page load -->
<!DOCTYPE html>
<html>
<head>
<title>Beacon Order Server - CRM</title>
<meta charset="utf-8">
<!--loag Assets  -->
<?php $this->load->view('pageassets');?>

</head>
<body>
	<!-- load navibar -->
<?php $this->load->view('navibar');?>

	<div class="container">
		<div class="jumbotron">
			<h1>Customer Service</h1>
			<p></p>
		</div>
		<div>
		<table class="table table-striped">
		<form name="user" method="POST" action="/studioshopplantform/index.php/CRM/Edit">
			<thead>
				<tr>
					<th>ID</th>
					<th><span class="glyphicon glyphicon-tag"></span> Name</th>
					<th><span class="glyphicon glyphicon-envelope"></span> E-mail</th>
					<th><span class="glyphicon glyphicon-edit"></span> Edit</th>
				</tr>
			</thead>
			<tbody>
				<?php
					foreach ($MemberList as $List) {
						$num = $List->id;
						echo "<tr><td>" . $List->id . "</td><td>" . $List->Name . "</td><td>" . $List->email . "</td><td>" . "<input type='submit' name='submit' class='btn btn-warning' value=$num ></td></tr>";
					}

				?>
			</tbody>
			</form>
		</table>
		</div>
	</div>
</body>
</html>