<!DOCTYPE html>
<html>
<head>
<title>Shop - Edit</title>
<meta charset="utf-8">
<!--loag bootstrap css  -->
<meta name="viewport" content="width=device-width, initial-scal=1">
<link rel="stylesheet"
	href="/BeaconOrderServer/assets/css/bootstrap.min.css">
<script src="/BeaconOrderServer/assets/js/jquery-1.11.3.min.js"></script>
<script src="/BeaconOrderServer/assets/js/bootstrap.min.js"></script>

</head>
<body>
	<!-- load navibar -->
<?php $this->load->view('navibar');?>

	<div class="container">
		<div class="jumbotron">
			<h1>Shop Edit</h1>
			<p></p>
		</div>
		<div class="form-horizontal">
			<a href="/BeaconOrderServer/index.php/Shop">Back to list</a>
			
			<?php
			$id = $_POST ["submit"];
			$servername = "localhost";
			$username = "root";
			$password = "";
			$dbname = "bryan_beaconorder";
			
			// Create connection
			$conn = new mysqli ( $servername, $username, $password, $dbname );
			
			// 資料回傳編碼設UTF8
			mysqli_set_charset ( $conn, "utf8" );
			
			// Check connection
			if ($conn->connect_error) {
				die ( "Connection failed: " . $conn->connect_error );
			}
			
			$sql = "SELECT * FROM `shop` WHERE Id = $id";
			$result = $conn->query ( $sql );
			
			if ($result->num_rows > 0) {
				echo "<table class=table>
					        <tr><th>ID</th><th>Name</th><th>Locate</th><th>Capacity</th><th>Area</th>
                				<th>BeaconUse</th><th>MenuId</th><th>Person</th><th>Contact</th><th>Note</th><tr>";
				// output data of each row
				while ( $row = $result->fetch_assoc () ) {
					$num = $row ["Id"];
					echo "<tr><td>" . $row ["Id"] . "</td><td>" . $row ["Name"] . "</td><td>" . $row ["Locate"] . "</td><td>" . $row ["Capacity"] . "</td><td>" . $row ["Area"] . "</td><td>" . $row ["BeaconUse"] . "</td><td>" . $row ["MenuId"] . "</td><td>" . $row ["Person"] . "</td><td>" . $row ["Contact"] . "</td><td>" . $row ["Note"];
				}
				echo "</table>";
			} else {
				echo "0 results";
			}
			
			$conn->close ();
			?>
			
			<hr>
			<form method="POST" action="/BeaconOrderServer/index.php/Shop/Delete">

				<div class="form-group">
					<label class="control-label col-md-2">Id</label>
					<div class="col-md-10">
	                <?php echo "<input class=form-control text-box single-line id=Id name=Id type=text value=$id>"?>
	                <span class="field-validation-valid text-danger"
							data-valmsg-for="Name" data-valmsg-replace="true"></span>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-offset-2 col-md-10">
						<input type="submit" name="Submit" class="btn btn-danger"
							value="Delete">
					</div>
				</div>
			</form>

			<hr>
			<form name="user" method="POST"
				action="/BeaconOrderServer/index.php/Shop/Update">
				<div class="form-group">
					<label class="control-label col-md-2">Id</label>
					<div class="col-md-10">
	                <?php echo "<input class=form-control text-box single-line id=Id name=Id type=text value=$id>"?>
	                <span class="field-validation-valid text-danger"
							data-valmsg-for="Name" data-valmsg-replace="true"></span>
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-md-2">Name</label>
					<div class="col-md-10">
						<input class="form-control text-box single-line" id="Name"
							name="Name" type="text" value=""> <span
							class="field-validation-valid text-danger" data-valmsg-for="Name"
							data-valmsg-replace="true"></span>
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-md-2">Locate</label>
					<div class="col-md-10">
						<input class="form-control text-box single-line" id="Locate"
							name="Locate" type="text" value=""> <span
							class="field-validation-valid text-danger"
							data-valmsg-for="Locate" data-valmsg-replace="true"></span>
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-md-2">Capacity</label>
					<div class="col-md-10">
						<input class="form-control text-box single-line" id="Capacity"
							name="Capacity" type="text" value=""> <span
							class="field-validation-valid text-danger"
							data-valmsg-for="Capacity" data-valmsg-replace="true"></span>
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-md-2">Area</label>
					<div class="col-md-10">
						<input class="form-control text-box single-line" id="Area"
							name="Area" type="text" value=""> <span
							class="field-validation-valid text-danger" data-valmsg-for="Area"
							data-valmsg-replace="true"></span>
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-md-2">BeaconUse</label>
					<div class="col-md-10">
						<input class="form-control text-box single-line" id="BeaconUse"
							name="BeaconUse" type="text" value=""> <span
							class="field-validation-valid text-danger"
							data-valmsg-for="BeaconUse" data-valmsg-replace="true"></span>
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-md-2">MenuId</label>
					<div class="col-md-10">
						<input class="form-control text-box single-line" id="MenuId"
							name="MenuId" type="text" value=""> <span
							class="field-validation-valid text-danger"
							data-valmsg-for="MenuId" data-valmsg-replace="true"></span>
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-md-2">Person</label>
					<div class="col-md-10">
						<input class="form-control text-box single-line" id="Person"
							name="Person" type="text" value=""> <span
							class="field-validation-valid text-danger"
							data-valmsg-for="Person" data-valmsg-replace="true"></span>
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-md-2">Contact</label>
					<div class="col-md-10">
						<input class="form-control text-box single-line" id="Contact"
							name="Contact" type="text" value=""> <span
							class="field-validation-valid text-danger"
							data-valmsg-for="Contact" data-valmsg-replace="true"></span>
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-md-2">Note</label>
					<div class="col-md-10">
						<input class="form-control text-box single-line" id="Note"
							name="Note" type="text" value=""> <span
							class="field-validation-valid text-danger" data-valmsg-for="Note"
							data-valmsg-replace="true"></span>
					</div>
				</div>

				<div class="form-group">
					<div class="col-md-offset-2 col-md-10">
						<input type="submit" name="Submit" class="btn btn-success"
							value="Edit Confirm">
					</div>
				</div>
			</form>
		</div>
	</div>
</body>
</html>