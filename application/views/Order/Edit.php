<!DOCTYPE html>
<html>
<head>
<title>Order Detail - Edit</title>
<meta charset="utf-8">
<!--loag Assets  -->
<?php $this->load->view('pageassets');?>

</head>
<body>
	<!-- load navibar -->
<?php $this->load->view('navibar');?>

	<div class="container">
		<div class="jumbotron">
			<h1>Order Detail Edit</h1>
			<p></p>
		</div>

		<div class="form-horizontal">
			<a href="/BeaconOrderServer/index.php/Order">Back to list</a>
			<?php
			$id = $_POST ["submit"];
			$servername = "localhost";
			$username = "root";
			$password = "";
			$dbname = "bryan_beaconorder";
			
			// Create connection
			$conn = new mysqli ( $servername, $username, $password, $dbname );
			
			// 資料回傳編碼設UTF8
			mysqli_set_charset ( $conn, "utf8" );
			
			// Check connection
			if ($conn->connect_error) {
				die ( "Connection failed: " . $conn->connect_error );
			}
			
			$sql = "SELECT * FROM `orderlist` WHERE Id = $id";
			$result = $conn->query ( $sql );
			
			if ($result->num_rows > 0) {
				?><table class="table">
				<thead>
					<tr>
						<th>ID</th>
						<th>Shop</th>
						<th>Product</th>
						<th>Count</th>
						<th>Price</th>
						<th>CeateTime</th>
						<th>CustomerEmail</th>
						<th>BeaconId</th>
						<th>ProductId</th>
						<th>ShopId</th>
						<th>CustomerId</th>
						<th>Complete</th>
					</tr>

				</thead>
				<tbody><?php
				// output data of each row
				while ( $row = $result->fetch_assoc () ) {
					$num = $row ["Id"];
					echo "<tr><td>" . $row ["Id"] . "</td><td>" . $row ["Shop"] . "</td><td>" . $row ["Product"] . "</td><td>" . $row ["Count"] . "</td><td>" . $row ["Price"] . "</td><td>" . $row ["CeateTime"] . "</td><td>" . $row ["CustomerEmail"] . "</td><td>" . $row ["BeaconId"] . "</td><td>" . $row ["ProductId"] . "</td><td>" . $row ["ShopId"] . "</td><td>" . $row ["CustomerId"] . "</td><td>" . $row ["Complete"] . "</td></tr>";
				}
				?></tbody>
			</table><?php
			} else {
				echo "0 results";
			}
			$conn->close ();
			?>
			
			<hr>
			<form method="POST"
				action="/BeaconOrderServer/index.php/Order/Delete">

				<div class="form-group">
					<label class="control-label col-md-2">Id</label>
					<div class="col-md-10">
	                <?php echo "<input class=form-control text-box single-line id=Id name=Id type=text value=$id>"?>
	                <span class="field-validation-valid text-danger"
							data-valmsg-for="Name" data-valmsg-replace="true"></span>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-offset-2 col-md-10">
						<input type="submit" name="Submit" class="btn btn-danger"
							value="Delete">
					</div>
				</div>
			</form>

			<hr>
			<form name="user" method="POST"
				action="/BeaconOrderServer/index.php/Order/Update">
				<div class="form-group">
					<label class="control-label col-md-2">Id</label>
					<div class="col-md-10">
	                <?php echo "<input class=form-control text-box single-line id=Id name=Id type=text value=$id>"?>
	                <span class="field-validation-valid text-danger"
							data-valmsg-for="Name" data-valmsg-replace="true"></span>
					</div>
				</div>

				<div class="form-group" id=Complete>
					<label class="control-label col-md-2">Complete</label>
					<div class="col-md-10">
						<input class="form-control text-box single-line" id="Complete"
							name="Complete" type="checkbox" value="1" checked="checked"> <span
							class="field-validation-valid text-danger"
							data-valmsg-for="Complete" data-valmsg-replace="true"></span>
					</div>
				</div>

				<div class="form-group">
					<div class="col-md-offset-2 col-md-10">
						<input type="submit" name="Submit" class="btn btn-success"
							value="Order Complete">
					</div>
				</div>
			</form>
		</div>
	</div>
</body>
</html>