<!DOCTYPE html>
<html>
<head>
<title>Order - Create</title>
<meta charset="utf-8">
<!--loag Assets  -->
<?php $this->load->view('pageassets');?>

</head>
<body>
	<!-- load navibar -->
<?php $this->load->view('navibar');?>

	<div class="container">
		<div class="jumbotron">
			<h1>Order Create</h1>
			
			<p></p>
		</div>
		<div class="form-horizontal">
			<a href="../Order">Back to list</a>
			<hr>
			<form name="user" method="POST" action="./Create">
			
				<?php
					foreach ($ProductTotal as $Total) {
						$totalnum = $Total->total;
					}

					foreach ($MenuProductList as $ProductList) {
						echo "<div class='form-group'><label class='control-label col-md-2'>" . $ProductList->Name . " - " . $ProductList->price . "</label><div class='col-md-2'>" . "<input class='form-control text-box single-line' type='number' name='PCount$totalnum' value='0' min='0' max='100'><p hidden><input class='form-control text-box single-line' type='text' name='PId$totalnum' value='$ProductList->Id'><input class='form-control text-box single-line' type='text' name='Price$totalnum' value='$ProductList->price'></p></div></div>";
						$totalnum = $totalnum - 1 ;
					}
				?>

				<div class="form-group">
					<div class="col-md-offset-2 col-md-10">
						<input type="submit" name="Submit" class="btn btn-success" value="Confirm">
					</div>
				</div>
			</form>
		</div>
	</div>
</body>
</html>