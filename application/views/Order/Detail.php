<!DOCTYPE html>
<html>
<head>
<title>Order Group - Edit</title>
<meta charset="utf-8">
<!--loag Assets  -->
<?php $this->load->view('pageassets');?>

</head>
<body>
	<!-- load navibar -->
<?php $this->load->view('navibar');?>

	<div class="container">

		<div class="jumbotron">
			<h1>Order Edit</h1>
			<p></p>
		</div>

		<div class="container">
			<a href="/BeaconOrderServer/index.php/Order">Back to list</a>
						<!-- OrderGroup List -->
			<table class="table">
				<thead>
					<tr>
						<th>ID</th>
						<th>CustomerId</th>
						<th>CustomerEmail</th>
						<th>Total_price</th>
						<th>Shop</th>
						<th>ShopId</th>
						<th>CeateTime</th>
						<th>Purchase</th>
						<th>Complete</th>
						<th>BeaconId</th>
					</tr>
				</thead>
				<tbody>
					<?php
						foreach ($OrderGroupList as $group) {
							if($group->Id == $_GET['OGId']) {
								echo "<tr class='danger'><td>" . $group->Id . "</td><td>" . $group->CustomerId . "</td><td>" . $group->CustomerEmail . "</td><td>" . $group->Total_price . "</td><td>" . $group->Shop . "</td><td>" . $group->ShopId . "</td><td>" . $group->CeateTime . "</td><td>" . $group->Purchase . "</td><td>" . $group->Complete . "</td><td>" . $group->BeaconId . "</td><td>" . "</td></tr>";
							}
						}
					?>
				</tbody>
			</table>
		</div>

		<hr>
		<div class="form-horizontal">
		<table class="table">
				<form name="user" method="POST" action="/BeaconOrderServer/index.php/Order/Edit">
					<thead>
						<tr>
							<th>ID</th>
							<th>Shop</th>
							<th>Product</th>
							<th>Count</th>
							<th>Price</th>
							<th>CeateTime</th>
							<th>CustomerEmail</th>
							<th>BeaconId</th>
							<th>ProductId</th>
							<th>ShopId</th>
							<th>CustomerId</th>
							<th>Complete</th>
							<th><span class="glyphicon glyphicon-edit"></span> Edit</th>
						</tr>
					</thead>
					<tbody>
					<!-- Order List -->
					<?php
						foreach ($OrderList as $List) {
							$num = $List->Id;
							if($List->GroupId == $_GET['OGId'])
							{
								if ($List->Complete == 0) {
									echo "<tr class='danger'><td>" . $List->Id . "</td><td>" . $List->Shop . "</td><td>" . $List->Product . "</td><td>" . $List->Count . "</td><td>" . $List->Price . "</td><td>" . $List->CeateTime . "</td><td>" . $List->CustomerEmail . "</td><td>" . $List->BeaconId . "</td><td>" . $List->ProductId . "</td><td>" . $List->ShopId . "</td><td>" . $List->CustomerId . "</td><td>" . $List->Complete . "</td><td>" . "<input type='submit' name='submit' class='btn btn-info' value=$num /></td></tr>";
								} else {
									echo "<tr class='danger'><td>" . $List->Id . "</td><td>" . $List->Shop . "</td><td>" . $List->Product . "</td><td>" . $List->Count . "</td><td>" . $List->Price . "</td><td>" . $List->CeateTime . "</td><td>" . $List->CustomerEmail . "</td><td>" . $List->BeaconId . "</td><td>" . $List->ProductId . "</td><td>" . $List->ShopId . "</td><td>" . $List->CustomerId . "</td><td>" . $List->Complete . "</td><td>" . "<input type='submit' name='submit' class='btn btn-info' value=$num /></td></tr>";
								}
							}
						}

					?>
					</tbody>
					</form>
				</table>
		</div>

		<hr>
		<form name="user" method="POST" action="/BeaconOrderServer/index.php/Order/UpdateGroup">
			<div class="form-group">
				<label class="control-label col-md-2">Id</label>
				<div class="col-md-10">
	                <?php $id = $_GET['OGId']; echo "<input class=form-control text-box single-line id=Id name=Id type=text value=$id>"?>
					
	                <span class="field-validation-valid text-danger"
						data-valmsg-for="Name" data-valmsg-replace="true"></span>
				</div>
			</div>

			<div class="form-group" id=Purchase>
				<label class="control-label col-md-2">Purchase</label>
				<div class="col-md-10">
					<input class="form-control text-box single-line" id="Purchase"
						name="Purchase" type="checkbox" value="1" checked="checked"><span
						class="field-validation-valid text-danger"
						data-valmsg-for="Complete" data-valmsg-replace="true"></span>
				</div>
			</div>

			<div class="form-group" id=Complete>
				<label class="control-label col-md-2">Complete</label>
				<div class="col-md-10">
					<input class="form-control text-box single-line" id="Complete"
						name="Complete" type="checkbox" value="1"><span
						class="field-validation-valid text-danger"
						data-valmsg-for="Complete" data-valmsg-replace="true"></span>
				</div>
			</div>

			<div class="form-group">
				<div class="col-md-offset-2 col-md-10">
					<input type="submit" name="Submit" class="btn btn-success"
						value="Confirm">
				</div>
			</div>
		</form>
	</div>
</body>
</html>