<!-- page load -->
<!DOCTYPE html>
<html>
<head>
<title>Beacon Order Server - Order</title>
<meta charset="utf-8">
<!-- auto refresh meta -->
<meta http-equiv="refresh" content="<?php echo $sec?>;URL='<?php echo $page?>'">

<!--loag Assets  -->
<?php $this->load->view('pageassets');?>

</head>
<body>
	<!-- load navibar -->
<?php $this->load->view('navibar');?>

	<div class="container">
		<div class="jumbotron">
		<h1>Order List</h1>
		<a href="/studioshopplantform/index.php/Order/Create" class="btn btn-primary btn-lg"><span class="glyphicon glyphicon-plus-sign"></span> Create</a>
			<table class="table">
				<thead>
					<tr>
					<th>Order Group Count</th>
					<th>List Count</th>
					</tr>
				</thead>
				<tbody>
					<?php
					if ($_SESSION['role'] == "staff")
					{
						foreach ($OrderGroupCount as $GroupCount) {
						echo "<tr><td>" . $GroupCount->count1 . "</td><td>" . $GroupCount->count2 . "</td></tr>";
						}
					}
					?>
				</tbody>
				</table>
		</div>

		<div class="container">
		<h2>Wait List</h2>
		<table class="table">
			<form name="user" method="GET" action="/studioshopplantform/index.php/Order/Detail">
			<thead>
				<tr>
				<th>ID</th>
				<th>CustomerId</th>
				<th><span class="glyphicon glyphicon-envelope"></span> CustomerEmail</th>
				<th><span class="glyphicon glyphicon-usd"></span> Total price</th>
				<th>Shop</th>
				<th>ShopId</th>
				<th><span class="glyphicon glyphicon-calendar"></span> CeateTime</th>
				<th><span class="glyphicon glyphicon-credit-card"></span> Purchase</th>
				<th><span class="glyphicon glyphicon-cutlery"></span> Complete</th>
				<th><span class="glyphicon glyphicon-screenshot"></span> Table Number</th>
				<th><span class="glyphicon glyphicon-edit"></span> Detail</th>
				</tr>
			</thead>
			<tbody>
				<?php
					foreach ($OrderGrouplist as $group) {
						$num = $group->Id;
						if($group->Purchase =="0" or $group->Complete =="0") {
							if($_SESSION['role'] =="staff")
							{
								echo "<tr class='danger'><td>" . $group->Id . "</td><td>" . $group->CustomerId . "</td><td>" . $group->CustomerEmail . "</td><td>" . $group->Total_price . "</td><td>" . $group->Shop . "</td><td>" . $group->ShopId . "</td><td>" . $group->CeateTime . "</td><td>" . $group->Purchase . "</td><td>" . $group->Complete . "</td><td>" . $group->BeaconId . "</td><td>" . "<input type='submit' name='OGId' class='btn btn-info' value=$num /></td></tr>";
							} 
							else if($_SESSION['role']=="member")
							{
								if($group->CustomerId == $_SESSION['userid'])
								{
									echo "<tr class='danger'><td>" . $group->Id . "</td><td>" . $group->CustomerId . "</td><td>" . $group->CustomerEmail . "</td><td>" . $group->Total_price . "</td><td>" . $group->Shop . "</td><td>" . $group->ShopId . "</td><td>" . $group->CeateTime . "</td><td>" . $group->Purchase . "</td><td>" . $group->Complete . "</td><td>" . $group->BeaconId . "</td><td>" . "<input type='submit' name='OGId' class='btn btn-info' value=$num /></td></tr>";

								}
							}
							
							
						}
					}
				?>
			</tbody>
			</form>
		</table>
		</div>

		<div class="container">
		<h2>Complete List</h2>
		<table class="table">
			<form name="user" method="POST" action="/BeaconOrderServer/index.php/Order/Detail">
			<thead>
				<tr>
				<th>ID</th>
				<th>CustomerId</th>
				<th><span class="glyphicon glyphicon-envelope"></span> CustomerEmail</th>
				<th><span class="glyphicon glyphicon-usd"></span> Total price</th>
				<th>Shop</th>
				<th>ShopId</th>
				<th><span class="glyphicon glyphicon-calendar"></span> CeateTime</th>
				<th><span class="glyphicon glyphicon-credit-card"></span> Purchase</th>
				<th><span class="glyphicon glyphicon-cutlery"></span> Complete</th>
				<th><span class="glyphicon glyphicon-screenshot"></span> Table Number</th>
				<th><span class="glyphicon glyphicon-edit"></span> Detail</th>
				</tr>
			</thead>
			<tbody>
				<?php
					foreach ($OrderGrouplist as $group) {
						$num = $group->Id;
						if ($group->Purchase =="1" and $group->Complete =="1") {
								echo "<tr class='success'><td>" . $group->Id . "</td><td>" . $group->CustomerId . "</td><td>" . $group->CustomerEmail . "</td><td>" . $group->Total_price . "</td><td>" . $group->Shop . "</td><td>" . $group->ShopId . "</td><td>" . $group->CeateTime . "</td><td>" . $group->Purchase . "</td><td>" . $group->Complete . "</td><td>" . $group->BeaconId . "</td><td>" . "<input type='submit' name='submit' class='btn btn-info' value=$num /></td></tr>";
						}
					}
				?>
			</tbody>
			</form>
		</table>
		</div>

	</div>
</body>
</html>