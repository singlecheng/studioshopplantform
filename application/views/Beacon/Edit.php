<!DOCTYPE html>
<html>
<head>
<title>Beacon - Edit</title>
<meta charset="utf-8">
<!--loag Assets  -->
<?php $this->load->view('pageassets');?>

</head>
<body>
	<!-- load navibar -->
<?php $this->load->view('navibar');?>

	<div class="container">
		<div class="jumbotron">
			<h1>Shop Edit</h1>
			<p></p>
		</div>
		<div class="form-horizontal">
			<a href="/BeaconOrderServer/index.php/Shop">Back to list</a>
			
			<?php
$id = $_POST["submit"];
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "bryan_beaconorder";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);

// 資料回傳編碼設UTF8
mysqli_set_charset($conn, "utf8");

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$sql = "SELECT `Id`, `Major`, `Minor`, `Name`, `Locate`, `Note` FROM `beacon` WHERE Id = $id";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    ?><table class="table">
				<tr>
					<th>ID</th>
					<th>Major</th>
					<th>Minor</th>
					<th><span class="glyphicon glyphicon-tag"></span> Name</th>
					<th><span class="glyphicon glyphicon-screenshot"></span> Locate</th>
					<th>Note</th>
				</tr>
		<?php
    // output data of each row
    while ($row = $result->fetch_assoc()) {
        $num = $row["Id"];
        echo "<tr><td>" . $row["Id"] . "</td><td>" . $row["Major"] . "</td><td>" .
                 $row["Minor"] . "</td><td>" . $row["Name"] . "</td><td>" .
                 $row["Locate"] . "</td><td>" . $row["Note"] . "</td></tr>";
    }
    echo "</table>";
} else {
    echo "0 results";
}

$conn->close();
?>
			
			<hr>
				<form method="POST"
					action="/BeaconOrderServer/index.php/Shop/Delete">

					<div class="form-group">
						<label class="control-label col-md-2">Id</label>
						<div class="col-md-10">
	                <?php echo "<input class=form-control text-box single-line id=Id name=Id type=text value=$id>"?>
	                <span class="field-validation-valid text-danger"
								data-valmsg-for="Name" data-valmsg-replace="true"></span>
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-offset-2 col-md-10">
							<input type="submit" name="Submit" class="btn btn-danger"
								value="Delete">
						</div>
					</div>
				</form>

				<hr>
				<form name="user" method="POST"
					action="/BeaconOrderServer/index.php/Shop/Update">
					<div class="form-group">
						<label class="control-label col-md-2">Id</label>
						<div class="col-md-10">
	                <?php echo "<input class=form-control text-box single-line id=Id name=Id type=text value=$id>"?>
	                <span class="field-validation-valid text-danger"
								data-valmsg-for="Name" data-valmsg-replace="true"></span>
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-2">Major</label>
						<div class="col-md-10">
							<input class="form-control text-box single-line" id="Major"
								name="Major" type="text" value=""> <span
								class="field-validation-valid text-danger"
								data-valmsg-for="Major" data-valmsg-replace="true"></span>
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-2">Minor</label>
						<div class="col-md-10">
							<input class="form-control text-box single-line" id="Minor"
								name="Minor" type="text" value=""> <span
								class="field-validation-valid text-danger"
								data-valmsg-for="Minor" data-valmsg-replace="true"></span>
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-2">Name</label>
						<div class="col-md-10">
							<input class="form-control text-box single-line" id="Name"
								name="Name" type="text" value=""> <span
								class="field-validation-valid text-danger"
								data-valmsg-for="Name" data-valmsg-replace="true"></span>
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-2">Locate</label>
						<div class="col-md-10">
							<input class="form-control text-box single-line" id="Locate"
								name="Locate" type="text" value=""> <span
								class="field-validation-valid text-danger"
								data-valmsg-for="Locate" data-valmsg-replace="true"></span>
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-2">Note</label>
						<div class="col-md-10">
							<input class="form-control text-box single-line" id="Note"
								name="Note" type="text" value=""> <span
								class="field-validation-valid text-danger"
								data-valmsg-for="Note" data-valmsg-replace="true"></span>
						</div>
					</div>

					<div class="form-group">
						<div class="col-md-offset-2 col-md-10">
							<input type="submit" name="Submit" class="btn btn-success"
								value="Edit Confirm">
						</div>
					</div>
				</form>


				</div>
				</div>

</body>
</html>