<!DOCTYPE html>
<html lang="en">
<head>
<title>Beacon Order Server - Beacon</title>
<meta charset="utf-8">
<!--loag Assets  -->
<?php $this->load->view('pageassets');?>

</head>
<body>
	<!-- load navibar -->
<?php $this->load->view('navibar');?>

	<div class="container">
		<div class="jumbotron">
			<h1>Beacon List</h1>
			<p></p>
			<a href="/BeaconOrderServer/index.php/Beacon/Create"
				class="btn btn-primary btn-lg"><span
				class="glyphicon glyphicon-plus-sign"></span> Create</a>
		</div>

		<div>
			<?php
    $servername = "localhost";
    $username = "root";
    $password = "";
    $dbname = "bryan_beaconorder";
    $dbc = "utf8";
    
    // Create connection
    $conn = new mysqli($servername, $username, $password, $dbname);
    mysqli_set_charset($conn, "utf8");
    // Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    
    $sql = "SELECT `Id`, `Major`, `Minor`, `Name`, `Locate`, `Note` FROM `beacon` WHERE 1";
    $result = $conn->query($sql);
    if ($result->num_rows > 0) {
        ?><table class="table table-striped">
				<form name="user" method="POST"
					action="/BeaconOrderServer/index.php/Beacon/Edit">
				
				
				<thead>
					<tr>
						<th>ID</th>
						<th>Major</th>
						<th>Minor</th>
						<th><span class="glyphicon glyphicon-tag"></span> Name</th>
						<th><span class="glyphicon glyphicon-screenshot"></span> Locate</th>
						<th>Note</th>
						<th>Edit</th>
					</tr>
				</thead>
				<tbody><?php
        // output data of each row
        while ($row = $result->fetch_assoc()) {
            $num = $row["Id"];
            echo "<tr><td>" . $row["Id"] . "</td><td>" . $row["Major"] .
                     "</td><td>" . $row["Minor"] . "</td><td>" . $row["Name"] .
                     "</td><td>" . $row["Locate"] . "</td><td>" . $row["Note"] .
                     "</td><td>" .
                     "<input type='submit' name='submit' class='btn btn-warning' value=$num ></td></tr>";
        }
        ?></tbody>
				</form>
			</table><?php
    } else {
        echo "0 results";
    }
    $conn->close();
    ?>
		
		
		</div>

	</div>

</body>
</html>