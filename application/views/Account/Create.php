<!DOCTYPE html>
<html>
<head>
<title>Account - Create</title>
<meta charset="utf-8">
<!--loag Assets  -->
<?php $this->load->view('pageassets');?>

</head>
<body>
	<!-- load navibar -->
<?php $this->load->view('navibar');?>

	<div class="container">
		<div class="jumbotron">
			<h1>Account Create</h1>
			<p></p>
		</div>
		<div class="form-horizontal">
			<a href="/BeaconOrderServer/index.php/Account/view">Back to list</a>
			<hr>

			<form name="user" method="POST"
				action="/BeaconOrderServer/index.php/Account/create">
				<div class="form-group">
					<label class="control-label col-md-2">Account Name</label>
					<div class="col-md-10">
						<input class="form-control text-box single-line" id="Accountname"
							name="Accountname" type="text" value=""> <span
							class="field-validation-valid text-danger"
							data-valmsg-for="Accountname" data-valmsg-replace="true"></span>
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-md-2">Account Password</label>
					<div class="col-md-10">
						<input class="form-control text-box single-line"
							id="Accountpassword" name="Accountpassword" type="text" value="">
						<span class="field-validation-valid text-danger"
							data-valmsg-for="Accountpassword" data-valmsg-replace="true"></span>
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-md-2" for="">Role</label>
					<div class="col-md-10">
						<select class="form-control" id="role" name="role">
							<option selected="selected" value="order">Order</option>
							<option value="shop">Shop</option>
							<option value="pro">Promotion</option>
							<option value="bea">Beacon</option>
							<option value="data">Report</option>
						</select> <span class="field-validation-valid text-danger"
							data-valmsg-for="Department3" data-valmsg-replace="true"></span>
					</div>
				</div>

				<div class="form-group">
					<div class="col-md-offset-2 col-md-10">
						<input type="submit" name="Submit" class="btn btn-danger"
							value="Confirm">
					</div>
				</div>
			</form>

		</div>
	</div>
</body>
</html>