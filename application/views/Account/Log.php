<!DOCTYPE html>
<html>
<head>
<title>Account - Log</title>
<meta charset="utf-8">
<!--loag Assets  -->
<?php $this->load->view('pageassets');?>

</head>
<body>
	<!-- load navibar -->
<?php $this->load->view('navibar');?>

	<div class="container">
		<div class="jumbotron">
			<h1>Log</h1>
			<p></p>
		</div>
		<a href="/BeaconOrderServer/index.php/Account/view">Back to list</a>
		<div>
		<table class="table table-striped">
				<form name="user" method="POST" action="">
					<thead>
						<tr>
							<th>ID</th>
							<th><span class="glyphicon glyphicon-tag"></span> Account Name</th>
							<th><span class="glyphicon glyphicon-import"></span> Action</th>
							<th><span class="glyphicon glyphicon-calendar"></span> Time</th>
						</tr>
					</thead>
					<tbody>
			<?php
			foreach ($LogList as $Log) {
				if ($Log->action == "Login") {
					echo "<tr class='info'><td>" . $Log->Id . "</td><td>" . $Log->username . "</td><td>" . $Log->action . "</td><td>" . $Log->time . "</td></tr>";
				} else if ($Log->action == "Logout") {
					echo "<tr class='success'><td>" . $Log->Id . "</td><td>" . $Log->username . "</td><td>" . $Log->action . "</td><td>" . $Log->time . "</td></tr>";
				} else if ($Log->action == "UpdateGroup" or $Log->action == "UpdateDetail") {
					echo "<tr class='warning'><td>" . $Log->Id . "</td><td>" . $Log->username . "</td><td>" . $Log->action . "</td><td>" . $Log->time . "</td></tr>";
				} else {
					echo "<tr class='danger'><td>" . $Log->Id . "</td><td>" . $Log->username . "</td><td>" . $Log->action . "</td><td>" . $Log->time . "</td></tr>";
				}
			}

			?>
			</tbody>
				</form>
			</table>
			</div>
	</div>
</body>
</html>