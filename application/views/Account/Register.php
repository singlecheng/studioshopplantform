<!DOCTYPE html>
<html lang="en">
<head>
<title>Studio Shop Plantform - Account Login</title>
<meta charset="utf-8">
<!--loag Assets  -->
<?php $this->load->view('pageassets');?>

</head>
<body>
	<nav class="navbar navbar-inverse">
		<div class="container-fluid">
			<div class="navbar-header">
				<a class="navbar-brand" href="/studioshopplantform/index.php">Studio Shop Plantform</a>
			</div>
		</div>
	</nav>

	<div class="container">
		<div class="jumbotron">
			<h1>
				<b>Studio Shop Plantform</b>
			</h1>

		</div>
	</div>
	
	<div class="container">
		<h2>Member Register</h2>
		<div class="form-horizontal">
			<hr>

			<form name="user" method="POST" action="">
				<div class="form-group">
					<label class="control-label col-md-2">Account Name</label>
					<div class="col-md-10">
						<input class="form-control text-box single-line" id="Account"
							name="Account" type="text" value=""> <span
							class="field-validation-valid text-danger"
							data-valmsg-for="Account" data-valmsg-replace="true"></span>
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-md-2">Account Password</label>
					<div class="col-md-10">
						<input class="form-control text-box single-line"
							id="Accountpassword" name="password" type="text" value="">
						<span class="field-validation-valid text-danger"
							data-valmsg-for="Accountpassword" data-valmsg-replace="true"></span>
					</div>
				</div>

                <div class="form-group">
					<label class="control-label col-md-2">Name</label>
					<div class="col-md-10">
						<input class="form-control text-box single-line" id="Name" name="Name" type="text" value=""> <span
							class="field-validation-valid text-danger"
							data-valmsg-for="Name" data-valmsg-replace="true"></span>
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-md-2">Email</label>
					<div class="col-md-10">
						<input class="form-control text-box single-line" id="Email" name="Email" type="text" value=""> <span
							class="field-validation-valid text-danger"
							data-valmsg-for="Email" data-valmsg-replace="true"></span>
					</div>
				</div>

				<div class="form-group">
					<div class="col-md-offset-2 col-md-10">
						<input type="submit" name="Submit" class="btn btn-success" value="Submit">
					</div>
				</div>
			</form>

		</div>
	</div>

	<!-- load footer -->
	<?php $this->load->view('pagefooter');?>
</body>
</html>
		