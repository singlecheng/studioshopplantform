<!-- page load -->
<!DOCTYPE html>
<html>
<head>
<title>Beacon Order Server - Account</title>
<meta charset="utf-8">
<!--loag Assets  -->
<?php $this->load->view('pageassets');?>

</head>
<body>
	<!-- load navibar -->
<?php $this->load->view('navibar');?>

	<div class="container">
		<div class="jumbotron">
			<h1>Account Management</h1>
			<p></p>
			<a href="/studioshopplantform/index.php/Account/Create" class="btn btn-info btn-lg" class="button"><span class="glyphicon glyphicon-plus-sign"></span> Create Account</a> 
			<a href="/studioshopplantform/index.php/Account/Log" class="btn btn-info btn-lg" class="button"><span class="glyphicon glyphicon-list-alt"></span> Account Log</a>
		</div>
		<div>
			<table class="table table-striped">
				<form name="user" method="POST" action="">
				<thead>
					<tr>
						<th>ID</th>
						<th><span class="glyphicon glyphicon-tag"></span> Account Name</th>
						<th><span class="glyphicon glyphicon-eye-open"></span> Role</th>
						<th><span class="glyphicon glyphicon-edit"></span> Edit</th>
					</tr>
				</thead>
				<tbody>
					<?php
						foreach ($UsersList as $User) {
							$num = $User->id;
							echo "<tr><td>" . $User->id . "</td><td>" . $User->username . "</td><td>" . $User->role . "</td><td>" . "<input type='submit' name='submit' class='btn btn-warning' value=$num ></td></tr>";
						}

					?>
				</tbody>
				</form>
			</table>
		</div>
	</div>
<?php $this->load->view('pagefooter');?>
</body>
</html> 