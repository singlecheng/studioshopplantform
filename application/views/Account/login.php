<!DOCTYPE html>
<html lang="en">
<head>
<title>Studio Shop Plantform - Account Login</title>
<meta charset="utf-8">
<!--loag Assets  -->
<?php $this->load->view('pageassets');?>

</head>
<body>
	<nav class="navbar navbar-inverse">
		<div class="container-fluid">
			<div class="navbar-header">
				<a class="navbar-brand" href="/studioshopplantform/index.php">Studio Shop Plantform</a>
			</div>
		</div>
	</nav>

	<div class="container">
		<div class="jumbotron">
			<h1>
				<b>Studio Shop Plantform</b>
			</h1>

		</div>
	</div>
	
	<div class="container">
		<h2>Server Login</h2>
		<form role="form" method="post" action="">
			<div class="form-group">
				<label for="email">Account:</label> <input type="text" class="form-control" name="username" id="username" value="" placeholder="Enter username">
			</div>
			<div class="form-group">
				<label for="pwd">Password:</label> <input type="password" class="form-control" name="password" id="password" value="" placeholder="Enter password">
			</div>
			<div class="checkbox">
				<label><input type="checkbox"> Remember me</label>
			</div>
			<button type="submit" class="btn btn-success">登入</button>
		</form>
	</div>

	<!-- load footer -->
	<?php $this->load->view('pagefooter');?>
</body>
</html>