<!DOCTYPE html>
<html>
<head>
<title>Promotion - Create</title>
<meta charset="utf-8">
<!--loag Assets  -->
<?php $this->load->view('pageassets');?>

</head>
<body>
	<!-- load navibar -->
<?php $this->load->view('navibar');?>

	<div class="container">
		<div class="jumbotron">
			<h1>Promotion Create</h1>
			<p></p>
		</div>
		<div class="form-horizontal">
			<a href="/BeaconOrderServer/index.php/Promotion">Back to list</a>
			<hr>

			<form name="user" method="POST"
				action="/BeaconOrderServer/index.php/Order/Create">
				<div class="form-group">
					<label class="control-label col-md-2">Name</label>
					<div class="col-md-10">
						<input class="form-control text-box single-line" id="Name"
							name="Name" type="text" value=""> <span
							class="field-validation-valid text-danger" data-valmsg-for="Name"
							data-valmsg-replace="true"></span>
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-md-2">Context</label>
					<div class="col-md-10">
						<input class="form-control text-box single-line" id="Context"
							name="Context" type="text" value=""> <span
							class="field-validation-valid text-danger"
							data-valmsg-for="Context" data-valmsg-replace="true"></span>
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-md-2">Beacon Id</label>
					<div class="col-md-10">
						<input class="form-control text-box single-line" id="BeaconId"
							name="BeaconId" type="text" value=""> <span
							class="field-validation-valid text-danger"
							data-valmsg-for="BeaconId" data-valmsg-replace="true"></span>
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-md-2">Shop</label>
					<div class="col-md-10">
						<input class="form-control text-box single-line" id="Shop"
							name="Shop" type="text" value=""> <span
							class="field-validation-valid text-danger" data-valmsg-for="Shop"
							data-valmsg-replace="true"></span>
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-md-2">Sale Price</label>
					<div class="col-md-10">
						<input class="form-control text-box single-line" id="SalePrice"
							name="SalePrice" type="text" value=""> <span
							class="field-validation-valid text-danger"
							data-valmsg-for="SalePrice" data-valmsg-replace="true"></span>
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-md-2">Shop Id</label>
					<div class="col-md-10">
						<input class="form-control text-box single-line" id="ShopId"
							name="ShopId" type="text" value=""> <span
							class="field-validation-valid text-danger"
							data-valmsg-for="ShopId" data-valmsg-replace="true"></span>
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-md-2">TIME</label>
					<div class="col-md-10">
						<input class="form-control text-box single-line" id="TIME"
							name="TIME" type="text" value=""> <span
							class="field-validation-valid text-danger" data-valmsg-for="TIME"
							data-valmsg-replace="true"></span>
					</div>
				</div>

				<div class="form-group">
					<div class="col-md-offset-2 col-md-10">
						<input type="submit" name="Submit" class="btn btn-default"
							value="確定新增">
					</div>
				</div>
			</form>

		</div>
	</div>
</body>
</html>