<!DOCTYPE html>
<html>
<head>
<title>Beacon Order Server - Data Analysis</title>
<meta charset="utf-8">
<!--loag Assets  -->
<?php $this->load->view('pageassets');?>

</head>
<body>
	<!-- load navibar -->
<?php $this->load->view('navibar');?>
	<div class="container">
		<div class="jumbotron">
			<h1>Data Analysis</h1>
			<a href="/BeaconOrderServer/index.php/ERP/ShopReport"
				class="btn btn-info btn-lg"><span class="glyphicon glyphicon-search"></span>店家業績</a>
			<a href="/BeaconOrderServer/index.php/ERP/BeaconReport"
				class="btn btn-info btn-lg"><span class="glyphicon glyphicon-search"></span>Beacon使用程度</a>

			<hr>
			<?php
	$sql = "SELECT SUM(`Price`) AS Total FROM orderlist";
	$result = $conn->query ( $sql );
	if ($result->num_rows > 0) {
		?>
						<table class="table table-striped">
				<thead>
					<tr>
						<th>Total Shop Revenue</th>
					</tr>
				</thead>
				<tbody><?php
		// output data of each row
		while ( $row = $result->fetch_assoc () ) {
			echo "<tr><td>" . $row ["Total"] . "</td></tr>";
		}
		?></tbody>
			</table><?php
	} else {
		echo "0 results";
	}
	?>
	</div>
	</div>
</body>
</html> 