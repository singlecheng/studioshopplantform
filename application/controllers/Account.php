<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Account extends CI_Controller
{

    public function index ()
    {
        // $this->load->database();
        // session_start();
        $this->load->model('AccountModels');
        $data['UsersList'] = $this->AccountModels->GetUsersList();

        if(isset($_SESSION['username'])){
            if ($_SESSION ['username'] == "admin") {
                $this->load->view('/Account/List', $data);
            }
            else{
                $id = $_SESSION ['username'];
                $sql = "INSERT INTO `accountlog` (`Id`, `username`, `action`, `time`) VALUES (NULL, '$id', 'Reject From Account', CURRENT_TIMESTAMP)";
                $this->db->query($sql);
                echo 'Access Denied';
                echo '<meta http-equiv=REFRESH CONTENT=2;url=/studioshopplantform/index.php>';
            }
        }
        else {
            echo 'Please Login';
	        echo '<meta http-equiv=REFRESH CONTENT=2;url=/studioshopplantform/Account/Login>';
        }
    }

    public function Login ()
    {
        $this->load->view('/Account/login');
        if($_SERVER["REQUEST_METHOD"] == "POST") {
            $id = $_POST['username'];
            $pw = $_POST['password'];
            $query = $this->db->query("SELECT * FROM `users` WHERE username = '$id'");
            foreach ($query->result() as $user)
            {
                    $id = $user->username;
                    $pw = $user->password;
                    $uid = $user->id;
                    $role = $user->role;
                    if($id != null && $pw != null && $user->username == $id && $user->password == $pw){
                        $_SESSION['username'] = $id;
                        $_SESSION['userid'] = $uid;
                        $_SESSION['role'] = $role;

                        $sql2 = "INSERT INTO `accountlog` (`Id`, `username`, `action`, `time`) VALUES (NULL, '$id', 'Login', CURRENT_TIMESTAMP)";
                        $sql = $this->db->query($sql2);
                        echo '<meta http-equiv=REFRESH CONTENT=1;url=/studioshopplantform/index.php>';
                    } else {
                        $sql3 = "INSERT INTO `accountlog` (`Id`, `username`, `action`, `time`) VALUES (NULL, '$id', 'Login Failed', CURRENT_TIMESTAMP)";
                        $sql = $this->db->query($sql3);
                        echo 'Access Denied';
                        echo '<meta http-equiv=REFRESH CONTENT=1;url=/studioshopplantform/Account/Login>';
                    }
            }  
        }
    }

    public function logout ()
    {
        // $this->load->database();
        // session_start();
        $id = $_SESSION['username'];
        
        $sql = "INSERT INTO `accountlog` (`Id`, `username`, `action`, `time`) VALUES (NULL, '$id', 'Reject From login', CURRENT_TIMESTAMP)";
        $this->db->query($sql);
        unset($_SESSION['username']);

        echo 'Logout . . . . . . ';
        echo '<meta http-equiv=REFRESH CONTENT=1;url=/studioshopplantform>';
    }

    public function Log ()
    {
        $this->load->model('AccountModels');
        $model['LogList'] = $this->AccountModels->GetLogList();
        $this->load->view('/Account/Log', $model);

    }

    public function Register ()
    {
        $this->load->view('/Account/Register');
        if($_SERVER["REQUEST_METHOD"] == "POST") {
            $ac = $_POST['Account'];
            $pw = $_POST['password'];
            $name = $_POST['Name'];
            $email = $_POST['Email'];
            
            $sql = "INSERT INTO `misdb`.`users` (`id`, `Name`, `username`, `email`, `role`, `password`, `activation`, `status`, `date`) VALUES (NULL, '$name', '$ac', '$email', 'member', '$pw', '', '0', CURRENT_TIMESTAMP);";
            $this->db->query($sql);
            echo "<script>";
            echo "alert(\"Register Success\");";
            echo "</script>";
            echo '<meta http-equiv=REFRESH CONTENT=1;url=/studioshopplantform/index.php/Account/Login>';
        }
    }

    public function Create ()
    {
        $this->load->view('/Account/Create');
        
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            $servername = "localhost";
            $username = "root";
            $password = "";
            $dbname = "bryan_beaconorder";
            
            $conn = new mysqli($servername, $username, $password, $dbname);
            mysqli_set_charset($conn, "utf8");
            
            $Accountname = $_POST["Accountname"];
            $Accountpassword = $_POST["Accountpassword"];
            $role = $_POST["role"];
            
            // Create connection
            $conn = new mysqli($servername, $username, $password, $dbname);
            // Check connection
            if ($conn->connect_error) {
                die("Connection failed: " . $conn->connect_error);
            }
            
            // 輸入字串編碼設UTF8
            mysqli_set_charset($conn, "utf8");
            
            $sql = "INSERT INTO `users` (`id`, `username`, `password`, `role`) VALUES ('', '$Accountname', '$Accountpassword', '$role')";
            
            if ($conn->query($sql) === TRUE) {
                echo "<script>";
                echo "alert(\"Account Created Success\");";
                echo "</script>";
                echo '<meta http-equiv=REFRESH CONTENT=1;url=//studioshopplantform/index.php/Account/view>';
            } else {
                echo "<script>";
                echo "alert(\"Account Create failed\");";
                echo "</script>";
                echo '<meta http-equiv=REFRESH CONTENT=1;url=//studioshopplantform/index.php/Account/create>';
            }
            
            $conn->close();
        }
    }

    public function Edit ()
    {

    }

   
}