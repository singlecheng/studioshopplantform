<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Order extends CI_Controller
{

    public function index ()
    {
        // $this->load->database();
        // session_start();
        $page = $_SERVER ['PHP_SELF'];
        $sec = "7";
        
        $this->load->model('OrderModels');
        $data['OrderGrouplist'] = $this->OrderModels->GetOrderGroupList();
        $data['OrderGroupCount'] = $this->OrderModels->GetOrderGroupCount();
        $data['page'] = $page;
        $data['sec'] = $sec;
        if(isset($_SESSION['username'])){
            if ($_SESSION ['role'] == "staff" or $_SESSION ['username'] == "admin") {
                $this->load->view('/Order/index', $data);
            }
            else if($_SESSION['role'] == "member")
            {
                $this->load->model('OrderModels');
                $data['OrderGrouplist'] = $this->OrderModels->GetOrderGroupList();
                $data['OrderGroupCount'] = $this->OrderModels->GetOrderGroupCount();
                $this->load->view('/Order/index', $data);
            }
            else{
                $id = $_SESSION ['username'];
                $sql = "INSERT INTO `accountlog` (`Id`, `username`, `action`, `time`) VALUES (NULL, '$id', 'Reject From Order', CURRENT_TIMESTAMP)";
                $this->db->query($sql);
                echo 'Access Denied';
                echo '<meta http-equiv=REFRESH CONTENT=2;url=./>';
            }
        }
        else {
            echo 'Please Login';
	        echo '<meta http-equiv=REFRESH CONTENT=2;url=./Account/Login>';
        }
        
    }

    public function Create ()
    {
        $this->load->model('OrderModels');
        $model['MenuProductList'] = $this->OrderModels->GetMenuProductList();
        $model['ProductTotal'] = $this->OrderModels->GetProductTotal();
        $model['OrderGroupID'] = $this->OrderModels->GetOrderGroupID();
        $this->load->view('/Order/Create', $model);

        foreach ($model['ProductTotal'] as $Total) {
            $totalnum = $Total->total;
        }

        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            $CustomerId = $_SESSION['userid'];
            $sql="INSERT INTO `misdb`.`ordergroup` (`Id`, `CustomerId`, `CustomerEmail`, `Total_price`, `Locate`, `Shop`, `ShopId`, `CeateTime`, `Purchase`, `Complete`, `BeaconId`) VALUES (NULL, '$CustomerId', NULL, '', NULL, NULL, NULL, CURRENT_TIMESTAMP, '0', '0', NULL)";
            $this->db->query($sql);
            foreach ($model['OrderGroupID'] as $GroupID) {
                $OrderGroupId = $GroupID->Id + 1;
            }

            foreach ($model['MenuProductList'] as $ProductList) {
                $CountString = "PCount";
                $PriceString = "Price";

                $CountString = $CountString . $totalnum;
                $PriceString = $PriceString . $totalnum;

                $Count = $_POST[$CountString];
                $Price = $_POST[$PriceString];
                
                $sql = "INSERT INTO `misdb`.`orderlist` (`Id`, `GroupId`, `Shop`, `Product`, `Count`, `Price`, `CeateTime`, `CustomerEmail`, `BeaconId`, `ProductId`, `ShopId`, `CustomerId`, `Complete`) VALUES (NULL, '$OrderGroupId', NULL, '$ProductList->Name', '$Count', '$Price', CURRENT_TIMESTAMP, NULL, NULL, '$ProductList->Id', NULL, NULL, NULL)";
                if($Count!=0)
                {
                    $this->db->query($sql);
                }
                $totalnum = $totalnum - 1;
            }
            echo "<script>";
            echo "alert(\"Create Success\");";
            echo "</script>";
            echo '<meta http-equiv=REFRESH CONTENT=2;url=../Order>';
        }
    }

    public function Detail ()
    {
        $this->load->model('OrderModels');
        $OrderGroupId = $_GET['OGId'];
        $model['OrderGroupList'] = $this->OrderModels->GetOrderGroupList();
        $model['GroupDetail'] = $this->OrderModels->GetOrderGroupDetail();
        $model['OrderList'] = $this->OrderModels->GetOrderDetail();
        $this->load->view('/Order/Detail', $model);

    }

    public function Edit ()
    {
        $this->load->view('/Order/Edit');
    }

    public function UpdateGroup ()
    {
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            $servername = "localhost";
            $username = "root";
            $password = "";
            $dbname = "bryan_beaconorder";
            
            $conn = new mysqli($servername, $username, $password, $dbname);
            mysqli_set_charset($conn, "utf8");
            
            $Id = $_POST["Id"];
            $Complete = $_POST["Complete"];
            $Purchase = $_POST["Purchase"];
            
            // Create connection
            $conn = new mysqli($servername, $username, $password, $dbname);
            // Check connection
            if ($conn->connect_error) {
                die("Connection failed: " . $conn->connect_error);
            }
            
            // 輸入字串編碼設UTF8
            mysqli_set_charset($conn, "utf8");
            
            $sql = "UPDATE `ordergroup` SET `Purchase` = '$Purchase', `Complete` = '$Complete' WHERE `ordergroup`.`Id` = $Id";
            
            if ($conn->query($sql) === TRUE) {
                echo "<link rel='stylesheet' href='/BeaconOrderServer/assets/css/bootstrap.min.css'>
                      <script src='/BeaconOrderServer/assets/js/jquery-1.11.3.min.js'></script>
                      <script src='/BeaconOrderServer/assets/js/bootstrap.min.js'></script>
                      <div class='alert alert-success'>
                        <strong>Success ! </strong> 
                      </div>";
                echo '<meta http-equiv=REFRESH CONTENT=1;url=/studioshopplantform/index.php/Order>';
            } else {
                echo "<script>";
                echo "alert(\"Edit Failed\");";
                echo "</script>";
                echo '<meta http-equiv=REFRESH CONTENT=1;url=/studioshopplantform/index.php/Order>';
            }
            
            $conn->close();
        }
        
        // Log處理
        $id = $_SESSION['username'];
        $servername = "localhost";
        $username = "root";
        $password = "";
        $dbname = "bryan_beaconorder";
        $conn = new mysqli($servername, $username, $password, $dbname);
        mysqli_set_charset($conn, "utf8");
        
        $sql2 = "INSERT INTO `accountlog` (`Id`, `username`, `action`, `time`) VALUES (NULL, '$id', 'UpdateGroup', CURRENT_TIMESTAMP)";
        $conn->query($sql2) === TRUE;
    }

    public function Update ()
    {
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            $servername = "localhost";
            $username = "root";
            $password = "";
            $dbname = "bryan_beaconorder";
            
            $conn = new mysqli($servername, $username, $password, $dbname);
            mysqli_set_charset($conn, "utf8");
            
            $Id = $_POST["Id"];
            $Complete = $_POST["Complete"];
            
            // Create connection
            $conn = new mysqli($servername, $username, $password, $dbname);
            // Check connection
            if ($conn->connect_error) {
                die("Connection failed: " . $conn->connect_error);
            }
            
            // 輸入字串編碼設UTF8
            mysqli_set_charset($conn, "utf8");
            
            $sql = "UPDATE `orderlist` SET `Complete` = '$Complete' WHERE `orderlist`.`Id` = $Id";
            
            if ($conn->query($sql) === TRUE) {
                echo "<link rel='stylesheet' href='/BeaconOrderServer/assets/css/bootstrap.min.css'>
                      <script src='/BeaconOrderServer/assets/js/jquery-1.11.3.min.js'></script>
                      <script src='/BeaconOrderServer/assets/js/bootstrap.min.js'></script>
                      <div class='alert alert-success'>
                        <strong>Success ! </strong> 
                      </div>";
                echo '<meta http-equiv=REFRESH CONTENT=1;url=/studioshopplantform/index.php/Order>';
            } else {
                echo "Edit Failed";
                echo '<meta http-equiv=REFRESH CONTENT=1;url=/studioshopplantform/index.php/Order/Edit>';
            }
            
            $conn->close();
        }
        // Log處理
        $id = $_SESSION['username'];
        $servername = "localhost";
        $username = "root";
        $password = "";
        $dbname = "bryan_beaconorder";
        $conn = new mysqli($servername, $username, $password, $dbname);
        mysqli_set_charset($conn, "utf8");
        
        $sql2 = "INSERT INTO `accountlog` (`Id`, `username`, `action`, `time`) VALUES (NULL, '$id', 'UpdateDetail', CURRENT_TIMESTAMP)";
        $conn->query($sql2) === TRUE;
    }

    public function Delete ()
    {
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            $servername = "localhost";
            $username = "root";
            $password = "";
            $dbname = "bryan_beaconorder";
            
            $conn = new mysqli($servername, $username, $password, $dbname);
            mysqli_set_charset($conn, "utf8");
            
            $Id = $_POST["Id"];
            
            // Create connection
            $conn = new mysqli($servername, $username, $password, $dbname);
            // Check connection
            if ($conn->connect_error) {
                die("Connection failed: " . $conn->connect_error);
            }
            
            // 輸入字串編碼設UTF8
            mysqli_set_charset($conn, "utf8");
            
            $sql = "DELETE FROM `orderlist` WHERE `orderlist`.`Id` = $Id";
            
            if ($conn->query($sql) === TRUE) {
                echo "<link rel='stylesheet' href='/studioshopplantform/assets/css/bootstrap.min.css'>
                      <script src='/studioshopplantform/assets/js/jquery-1.11.3.min.js'></script>
                      <script src='/studioshopplantform/assets/js/bootstrap.min.js'></script>
                      <div class='alert alert-success'>
                        <strong>Delete Success ! </strong> 
                      </div>";
                echo '<meta http-equiv=REFRESH CONTENT=1;url=/studioshopplantform/index.php/Order>';
            } else {
                echo "<script>";
                echo "alert(\"刪除資料失敗，請重新嘗試\");";
                echo "</script>";
                echo '<meta http-equiv=REFRESH CONTENT=1;url=/studioshopplantform/index.php/Order>';
            }
            
            $conn->close();
        }
        
        // Log處理
        $id = $_SESSION['username'];
        $servername = "localhost";
        $username = "root";
        $password = "";
        $dbname = "bryan_beaconorder";
        $conn = new mysqli($servername, $username, $password, $dbname);
        mysqli_set_charset($conn, "utf8");
        
        $sql2 = "INSERT INTO `accountlog` (`Id`, `username`, `action`, `time`) VALUES (NULL, '$id', 'DeleteOrder', CURRENT_TIMESTAMP)";
        $conn->query($sql2) === TRUE;
    }
}