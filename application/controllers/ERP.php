<?php
defined('BASEPATH') or exit('No direct script access allowed');

class ERP extends CI_Controller
{

    public function index ()
    {
        $config['charset'] = "UTF-8";
        
        
        if(isset($_SESSION['username'])){
            if ($_SESSION ['username'] == "data" or $_SESSION ['username'] == "admin") {
                $this->load->view('/ERP/index');
            }
            else{
                $id = $_SESSION ['username'];
                $sql = "INSERT INTO `accountlog` (`Id`, `username`, `action`, `time`) VALUES (NULL, '$id', 'Reject From Data Analysis', CURRENT_TIMESTAMP)";
                $this->db->query($sql);
                echo 'Access Denied';
                echo '<meta http-equiv=REFRESH CONTENT=2;url=./index.php>';
            }
        }
        else {
            echo 'Please Login';
	        echo '<meta http-equiv=REFRESH CONTENT=2;url=./Account/Login>';
        }
    }

    public function ShopReport ()
    {
        $config['charset'] = "UTF-8";
        $this->load->view('/ERP/ShopReport');
    }

    public function BeaconReport ()
    {
        $config['charset'] = "UTF-8";
        $this->load->view('/ERP/BeaconReport.php');
    }
}
