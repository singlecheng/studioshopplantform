<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Menu extends CI_Controller
{

    public function index ()
    {
        $this->load->model('MenuModels');
        $model['MenuList'] = $this->MenuModels->GetMenuList();
        $this->load->view('/Menu/index', $model);
    }

    public function Create()
    {
        $this->load->view('/Menu/Create');

        if ($_SERVER["REQUEST_METHOD"] == "POST"){
            $name = $_POST['Name'];
            $type = $_POST['Type'];
            $price = $_POST['Price'];

            $sql = "INSERT INTO `misdb`.`menuproduct` (`Id`, `Name`, `type`, `price`, `Shop`, `ShopId`) VALUES (NULL, '$name', '$type', '$price', NULL, '1')";
            $this->db->query($sql);
            
            echo "<script>";
            echo "alert(\"Create Success\");";
            echo "</script>";
            echo '<meta http-equiv=REFRESH CONTENT=2;url=/studioshopplantform/index.php/Menu>';
        }
    }
}
